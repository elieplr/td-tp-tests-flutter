import 'package:flutter/material.dart';

import 'MyListItem.dart';

class MyListView extends StatelessWidget{
  const MyListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(8),
      itemExtent: 80.0, //forces the children to have the given extent in the scroll direction.
      children: <Widget>[
        /*
        Text('Element 1'),
        Text('Element 2'),
        Text('Element 3')
        */
        const Card(child:ListTile(title: Text('One-line ListTile'))),
        const Card(child:ListTile(leading: FlutterLogo(),title: Text('One-line with leading widget'),),),
        const Card(child:ListTile(title: Text('One-line with trailing widget'), trailing: Icon(Icons.more_vert),),),
        const Card(child:ListTile(
          leading: FlutterLogo(),
          title: Text('One-line with both widgets'),
          trailing: Icon(Icons.more_vert),
        ),),
        const Card(child: ListTile(
          title: Text('One-line dense ListTile'),
          dense: true,
        ),),
        const Card(child: ListTile(
          leading: FlutterLogo(size: 56.0),
          title: Text('Two-line ListTile'),
          subtitle: Text('Here is a second line'),
          trailing: Icon(Icons.more_vert),
        ),),
        const Card(child:ListTile(
          leading: FlutterLogo(size: 72.0),
          title: Text('Three-line ListTile'),
          subtitle: Text(
              'A sufficiently long subtitle warrants three lines.'
          ),
          trailing: Icon(Icons.more_vert),
          isThreeLine: true,
        ),),
        MyListItem(
          subtitle: 'Flutter',
          count: 999000,
          image: Container(
            decoration: const BoxDecoration(color: Colors.blue),
          ),
          title: 'Flutter exemple',
        ),
        MyListItem(
          subtitle: 'Android',
          count: 884000,
          image: Container(
            decoration: const BoxDecoration(color: Colors.yellow),
          ),
          title: 'Android exemple',
        ),
      ],
    );
  }
}
