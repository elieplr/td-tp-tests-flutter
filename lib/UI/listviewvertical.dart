import 'package:flutter/material.dart';

class ListViewVertical extends StatelessWidget {
  final List<int> numbers = [1, 2, 3, 5, 8, 13, 21, 34, 55, 15,66,155,454,121,1,235,444,3,222,1111];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Vertical ListView'),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 24.0),
        height: MediaQuery.of(context).size.height ,
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: numbers.length, itemBuilder: (context, index) {
          return Container(
            width: MediaQuery.of(context).size.width * 0.6,
            child: Card(
              color: Colors.blue,
              child: Container(
                child: Center(child: Text(numbers[index].toString(), style: TextStyle(color: Colors.white, fontSize: 36.0),)),
              ),
            ),
          );
        }),
      ),
    );
  }
}