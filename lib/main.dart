import 'package:flutter/material.dart';

import 'package:tp1/UI/listviewdividetiles.dart';
import 'package:tp1/UI/listviewhorizontale.dart';
import 'package:tp1/UI/listviewvertical.dart';
import 'package:tp1/UI/listviewwithitembuilder.dart';
import 'package:tp1/UI/mybuilder.dart';
import 'package:tp1/UI/navigation.dart';

import 'UI/MyListView.dart';


void main() {
  //runApp(const MyApp());
  //runApp(MaterialApp(home:ListViewHorizontale()));
  //runApp(MaterialApp(home:ListViewVertical()));
  //runApp(MaterialApp(home:ListViewDivideTiles()));
  //runApp(MaterialApp(home:MyListViewWithItemBuilder()));
  //runApp(const MaterialApp(title: 'Navigation Basics', home: FirstRoute(),));
  runApp(
    MaterialApp(
      title: 'Named Routes Demo',
      // Start the app with the "/" named route. In this case, the app starts
      // on the FirstScreen widget.
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) => const FirstScreen(),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/second': (context) => const SecondScreen(),
      },
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ListView Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: const Text("ListView Demo"),
          ),
          body: ListTileTheme(
              contentPadding: const EdgeInsets.all(15),
              iconColor: Colors.red,
              textColor: Colors.black54,
              tileColor: Colors.yellow[100],
              style: ListTileStyle.list,
              dense: true,
              child: MyBuilder())
      ),
    );
  }
}



